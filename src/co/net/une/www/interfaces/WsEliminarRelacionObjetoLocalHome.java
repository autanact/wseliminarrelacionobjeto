/*
 * Generated by XDoclet - Do not edit!
 */
package co.net.une.www.interfaces;

/**
 * Local home interface for WsEliminarRelacionObjeto.
 * @xdoclet-generated at ${TODAY}
 * @copyright The XDoclet Team
 * @author XDoclet
 * @version ${version}
 */
public interface WsEliminarRelacionObjetoLocalHome
   extends javax.ejb.EJBLocalHome
{
   public static final String COMP_NAME="java:comp/env/ejb/WsEliminarRelacionObjetoLocal";
   public static final String JNDI_NAME="ejb/WsEliminarRelacionObjetoLocal";

   public co.net.une.www.interfaces.WsEliminarRelacionObjetoLocal create()
      throws javax.ejb.CreateException;

}
