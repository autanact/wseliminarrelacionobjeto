
/**
 * WsEliminarRelacionObjetoSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsEliminarRelacionObjetoSkeleton java skeleton for the axisService
     */
    public class WsEliminarRelacionObjetoSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsEliminarRelacionObjetoLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param dataset
                                     * @param tabla
                                     * @param codigoRegistro
                                     * @param relacion
         */
        

                 public co.net.une.www.gis.WsEliminarRelacionObjetoRSType eliminarRelacionObjeto
                  (
                  java.lang.String dataset,java.lang.String tabla,java.lang.String codigoRegistro,java.lang.String relacion
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("dataset",dataset);params.put("tabla",tabla);params.put("codigoRegistro",codigoRegistro);params.put("relacion",relacion);
		try{
		
			return (co.net.une.www.gis.WsEliminarRelacionObjetoRSType)
			this.makeStructuredRequest(serviceName, "eliminarRelacionObjeto", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    